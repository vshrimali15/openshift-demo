package com.shrimali.openshift.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shrimali.openshift.model.Employee;

@RestController
public class EmployeeController {

	@GetMapping("/emp")
	public List<Employee> list() {
		List<Employee> employees = Arrays.asList(new Employee(1, "vinay", 25000), new Employee(2, "kumar", 30000),
				new Employee(3, "shrimali", 35000));
		return employees;
	}
}
